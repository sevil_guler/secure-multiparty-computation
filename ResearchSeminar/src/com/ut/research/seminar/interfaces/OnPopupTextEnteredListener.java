package com.ut.research.seminar.interfaces;

public interface OnPopupTextEnteredListener {	
	public abstract void onItemEntered(String value);
}
