package com.ut.research.seminar.interfaces;

public interface OnPopupItemClickListener {
	public abstract void onItemClick(int itemId);
	
}
