/*
 * Copyright (c) 2014 Sevil GULER
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ut.researchseminar.activities;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import com.example.researchseminar.R;
import com.ut.research.seminar.interfaces.OnCustomAlertViewClickListener;
import com.ut.research.seminar.interfaces.OnPopupTextEnteredListener;
import com.ut.researchseminar.utils.BluetoothChatService;
import com.ut.researchseminar.utils.CustomAlertView;
import com.ut.researchseminar.utils.SecretPopup;
import com.ut.researchseminar.utils.TranscriptPopup;

import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class InitiatorActivity extends ActionBarActivity {

	private static  StringBuffer mOutStringBuffer;
	static LinearLayout connectLayout;
	static LinearLayout operationLayout;
	static LinearLayout layout;
	static LinearLayout bluetoothLayout;
	static ImageView bluetooth=null;
	static TextView addition;
	static TextView cancel;
	static TextView multiplication;
	static BluetoothAdapter mBluetoothAdapter=null;
	public boolean isSendFirstMessage=false;

	boolean isStarterForConnection=false;

	private static final int REQUEST_Turn_On_Discoverable = 3;
	static TextView textBlu;
	ArrayList<String> mArray;
	UUID MY_UUID;
	static Context context;
	private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
	private static final String TAG = "BluetoothChat";
	private static final boolean D = true;

	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;


	public static final String DEVICE_NAME = "DEVICE_NAME";
	public static final String TOAST = "TOAST";
	int s1_t1=-1;
	int s2_t2=-1;
	int w1=-1;
	int w2=-1;
	int w3=-1;

	// Intent request codes
	private static final int REQUEST_CONNECT_DEVICE = 1;
	private static final int REQUEST_ENABLE_BT = 2;
	public static final String RESULT = "RESULT";
	public static final String IS_ADDITION = "IS_ADDITION";
	private static BluetoothChatService mChatService = null;
	// Name of the connected device
	private String mConnectedDeviceName = null;
	private int watcherDevice=-1;
	private int secondDevice=-1;
	static ArrayList<BluetoothDevice> devices=new ArrayList<BluetoothDevice>();
	LinearLayout content;
	boolean messageIsReady=false;
	boolean isAddition=false;

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(modeChangedReceiver);
		unregisterReceiver(mReceiver);
		if (mChatService != null) mChatService.stop();

	}
	@Override
	public synchronized void onResume() {
		super.onResume();
		if (mChatService != null) {

			if (mChatService.getState() == BluetoothChatService.STATE_NONE) {
				mChatService.start();
			}
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		if (mBluetoothAdapter == null){

		}else{
			if (!mBluetoothAdapter.isEnabled()) {
				Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
				// Otherwise, setup the chat session
			} else {
				if (mChatService == null) setupChat();
			}
			if (mBluetoothAdapter.isEnabled()){
				if (mChatService == null) setupChat();
				if(mBluetoothAdapter.isDiscovering()){
					bluetooth.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_location_found));
				}else{

					bluetooth.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_location_off));
				}
			} 
		}
	}
	private void setupChat() {
		mChatService = new BluetoothChatService(this, mHandler);
		mOutStringBuffer = new StringBuffer("");

	}
	private final Handler mHandler = new Handler() {
		private String messages;

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_STATE_CHANGE:

				switch (msg.arg1) {
				case BluetoothChatService.STATE_CONNECTED:
					setStatus(getString(R.string.title_connected_to, mConnectedDeviceName));
					isStarterForConnection=false;

					break;
				case BluetoothChatService.STATE_CONNECTING:
					setStatus(R.string.title_connecting);
					break;
				case BluetoothChatService.STATE_LISTEN:
				case BluetoothChatService.STATE_NONE:
					setStatus(R.string.title_not_connected);
					break;
				}
				break;
			case MESSAGE_WRITE:

				if(mChatService.isSecond && !isSendFirstMessage){
					//isSecond,is initiator, is watcher, initiator address,the watcher address,the value,isAddition,result,isSenderInitiator
					//mChatService.start();
					isSendFirstMessage=true;
					messageIsReady=true;
					isStarterForConnection=true;

					mChatService.connectSecond(mBluetoothAdapter.getRemoteDevice(mChatService.getWatcherAddress()),false);
					if(isAddition){
						messages="0,0,1,"+mChatService.getInitiatorAddress()+","+mChatService.getWatcherAddress()+","+String.valueOf(t3)+","+"1"+","+String.valueOf((t2+s2)%100)+","+"0";
					}
					else{
						w2=(s2*t2+s2*t1+s1*t2)%1000;
						messages="0,0,1,"+mChatService.getInitiatorAddress()+","+mChatService.getWatcherAddress()+","+String.valueOf(t3)+","+"0"+","+String.valueOf(w2)+","+"0"+","+String.valueOf(t2);

					}

				}

				break;
			case MESSAGE_READ:
				byte[] readBuf = (byte[]) msg.obj;
				// construct a string from the valid bytes in the buffer
				String readMessage = new String(readBuf, 0, msg.arg1);
				String[] split=readMessage.split(",");
				//isSecond,is initiator, is watcher, initiator address,the watcher address,the value,isAddition,result,isSenderInitiator

				if(split.length==1){

					if(mChatService.isInitiator() && !isSendFirstMessage){

						isSendFirstMessage=true;
						String messages="";
						if(isAddition){
							messages="0,0,1,"+mBluetoothAdapter.getAddress()+","+devices.get(watcherDevice)+","+String.valueOf(s3)+","+"1"+","+String.valueOf((s1+t1)%100)+","+"1";
						}
						else{
							w1=(s1*t1+s1*t3+s3*t1)%1000;
							messages="0,0,1,"+mBluetoothAdapter.getAddress()+","+devices.get(watcherDevice)+","+String.valueOf(s3)+","+"0"+","+String.valueOf(w1)+","+"1"+","+ String.valueOf(s2);

						}
						InitiatorActivity.sendMessage(messages);

					}
					else if(mChatService.isSecond){
						isStarterForConnection=false;

					}
					reset();
					Toast.makeText(getApplicationContext(), "Calculation is completed. Guesser is in the Game.", Toast.LENGTH_SHORT).show();
				}
				else{
					if(split[1].equals("0")){
						mChatService.setInitiator(false);
					}
					else{
						t1=Integer.parseInt(split[5]);
						mChatService.setInitiator(true);
						if(!isAddition){
							t3=Integer.parseInt(split[9]);
						}


					}
					mChatService.setInitiatorAddress(split[3]);
					mChatService.setWatcherAddress(split[4]);

					final String isAdd=split[6];

					if(isAdd.equals("0")){
						isAddition=false;
					}
					else{
						isAddition=true;
					}
					if(split[2].equals("0")){
						mChatService.setWatcher(false);
					}
					else{

						if(split[8].equals("0")){
							mChatService.setInitiator(false);
							t3=Integer.parseInt(split[5]);
							if(isAddition){
								s2_t2=Integer.parseInt(split[7]);
							}
							else{
								w2=Integer.parseInt(split[7]);
								t2=Integer.parseInt(split[9]);
							}

							InitiatorActivity.sendMessage("stop");
							messages="0";
							messageIsReady=true;

							isStarterForConnection=true;
							mChatService.connectSecond(mBluetoothAdapter.getRemoteDevice(mChatService.getInitiatorAddress()), false);


						}
						else{
							s3=Integer.parseInt(split[5]);
							if(isAddition){
								s1_t1=Integer.parseInt(split[7]);
							}
							else{
								w1=Integer.parseInt(split[7]);
								s2=Integer.parseInt(split[9]);
							}

						}

						mChatService.setWatcher(true);
						if(isAddition){
							if(s1_t1!=-1 && s2_t2!=-1){
								InitiatorActivity.sendMessage("stop");
								guessStarts();
							}
						}
						else{
							if(w1!=-1 && w2!=-1){
								InitiatorActivity.sendMessage("stop");
								guessStartsForMul();
							}
						}
					}

					if(split[0].equals("0")){
						mChatService.setSecond(false);
					}
					else{
						mChatService.setSecond(true);
						s2=Integer.parseInt(split[5]);
						if(!isAddition){
							s1=Integer.parseInt(split[9]);
						}
						final SecretPopup secretPopup = new SecretPopup(InitiatorActivity.context);
						secretPopup.setTitle(getResources().getString(R.string.play));
						secretPopup.setContain(getResources().getString(R.string.t));
						secretPopup.show(content);
						secretPopup.setOnPopupTextEnteredListener(new OnPopupTextEnteredListener(){

							@Override
							public void onItemEntered(String value) {
								int t=Integer.parseInt(value)%100;
								Random ran = new Random();
								isSendFirstMessage=false;

								String message="";


								//mChatService.connectSecond(mBluetoothAdapter.getRemoteDevice(mChatService.getInitiatorAddress()), false);

								//isSecond,is initiator, is watcher, initiator address,the watcher address,the value,isAddition,result,isSenderInitiator

								if(isAddition){
									t1 = ran.nextInt(100);
									t2=ran.nextInt(100);
									t3=(t-t1-t2)%100;
									if (t3<0) t3 += 100;
									message="0,1,0,"+mChatService.getInitiatorAddress()+","+mChatService.getWatcherAddress()+","+String.valueOf(t1)+","+"1"+","+String.valueOf(0)+","+"0";
								}
								else{
									t1 = ran.nextInt(1000);
									t2=ran.nextInt(1000);
									t3=(t-t1-t2)%1000;
									if (t3<0) t3 += 1000;
									//isSecond,is initiator, is watcher, initiator address,the watcher address,the value,isAddition,result,isSenderInitiator, secondValue

									message="0,1,0,"+mChatService.getInitiatorAddress()+","+mChatService.getWatcherAddress()+","+String.valueOf(t1)+","+"0"+","+String.valueOf(0)+","+"0"+","+String.valueOf(t3);

								}
								InitiatorActivity.sendMessage(message);
							}

						});

					}
				}


				break;
			case MESSAGE_DEVICE_NAME:

				isStarterForConnection=false;
				mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
				Toast.makeText(getApplicationContext(), "Connected to "
						+ mConnectedDeviceName, Toast.LENGTH_SHORT).show();

				if(mChatService.isInitiator()){
					operationLayout.setVisibility(View.VISIBLE);
					layout.setVisibility(View.GONE);


				}
				if(mChatService.isWatcher){
					if(BluetoothChatService.lastMessage){}
				}
				if(messageIsReady){
					InitiatorActivity.sendMessage(messages);
					messageIsReady=false;
				}
				break;
			case MESSAGE_TOAST:
				if(isStarterForConnection && msg.getData().getString(TOAST).equals("Unable to connect device")){
					mChatService.connectSecond(mChatService.getCurrentDevice(),mChatService.isInitiator());
				}
				break;
			}
		}
	};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_initiator);
		context=this.getApplication().getApplicationContext();
		registerReceiver(modeChangedReceiver, 
				new IntentFilter(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED));
		mArray=new ArrayList<String>();
		IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		registerReceiver(mReceiver, filter);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
			.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	protected void reset() {
		mChatService.cancelOperation();
		mChatService.start();
		isSendFirstMessage=false;
		isStarterForConnection=false;
		messageIsReady=false;
		operationLayout.setVisibility(View.GONE);
		layout.setVisibility(View.VISIBLE);
		s1_t1=-1;
		s2_t2=-1;
		w1=-1;
		w2=-1;
		w3=-1;
		isAddition=false;
	}
	int u3=-1;
	int allResult=-1;

	protected void guessStarts() {

		final TranscriptPopup popup = new TranscriptPopup(InitiatorActivity.context,true,false);
		popup.setP1_send(String.valueOf(s3));
		popup.setP2_result(String.valueOf(s2_t2));
		popup.setP1_result(String.valueOf(s1_t1));
		popup.setP2_send(String.valueOf(t3));
		u3=(s3+t3)%100;
		popup.setP3_result(String.valueOf(u3));
		allResult=(s2_t2+s1_t1+u3)%100;
		popup.setAll_result(String.valueOf(allResult));

		popup.show(content);
		popup.setOnPopupTextEnteredListener(new OnPopupTextEnteredListener(){

			@Override
			public void onItemEntered(String value) {
				Intent intent=new Intent(InitiatorActivity.this, DiagramActivity.class);
				intent.putExtra(InitiatorActivity.IS_ADDITION, true);
				intent.putExtra(RESULT, allResult);
				reset();
				startActivity(intent);
			}

		});
	}

	protected void guessStartsForMul() {

		final TranscriptPopup popup = new TranscriptPopup(InitiatorActivity.context,false,false);
		popup.setP1_send('('+String.valueOf(s3)+','+String.valueOf(s2)+')');
		popup.setP2_result(String.valueOf(w2));
		popup.setP1_result(String.valueOf(w1));
		popup.setP2_send('('+String.valueOf(t3)+','+String.valueOf(t2)+')');
		w3=(s3*t3 + s3*t2 + s2*t3)%1000;
		popup.setP3_result(String.valueOf(w3));
		allResult=(w2+w1+w3)%1000;
		popup.setAll_result(String.valueOf(allResult));

		popup.show(content);
		popup.setOnPopupTextEnteredListener(new OnPopupTextEnteredListener(){

			@Override
			public void onItemEntered(String value) {
				String[] split=value.split(",");
				int valueInt=Integer.parseInt(split[0])*Integer.parseInt(split[1]);
				if(valueInt%1000!=allResult){
					final CustomAlertView ourNicePopupC = new CustomAlertView(getApplicationContext(),0);
					ourNicePopupC.setTitle(getResources().getString(R.string.guesser));
					ourNicePopupC.setWarn(String.format(getResources().getString(R.string.error_mult,value,allResult)));
					ourNicePopupC.show(content);
					ourNicePopupC.setOnCustomAlertViewClickListener(new OnCustomAlertViewClickListener(){

						@Override
						public void onItemClickLeave() {
							popup.show(content);
						}

					});
				}
				else{
					Intent intent=new Intent(InitiatorActivity.this,DiagramActivity.class);
					intent.putExtra(InitiatorActivity.IS_ADDITION, false);
					intent.putExtra(RESULT, allResult);
					reset();
					startActivity(intent);
				}
			}

		});
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.initiator, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == R.id.action_settings) {
			reset();
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_initiator,
					container, false);
			connectLayout=(LinearLayout)rootView.findViewById(R.id.search);
			ImageView connect=(ImageView)connectLayout.findViewById(R.id.image);
			connect.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_search));
			TextView text=(TextView)connectLayout.findViewById(R.id.text);
			text.setText(R.string.connect);

			mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			connectLayout.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					Intent serverIntent = new Intent(InitiatorActivity.context, DeviceListActivity.class);
					getActivity().startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
				}});

			bluetoothLayout=(LinearLayout)rootView.findViewById(R.id.openBluetooth);
			bluetooth=(ImageView)bluetoothLayout.findViewById(R.id.image);

			if (mBluetoothAdapter == null) {

			}
			if (mBluetoothAdapter.getScanMode()!=BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE){

				bluetooth.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_location_off));
			}
			else{
				bluetooth.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_location_found));
			}
			bluetoothLayout.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {

					if (mBluetoothAdapter.getScanMode()!=BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE){

						Intent discoverableIntent = new
								Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
						discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 120);
						getActivity().startActivity(discoverableIntent);
					}



				}


			});			
			textBlu=(TextView)bluetoothLayout.findViewById(R.id.text);
			textBlu.setText(R.string.bluetooth);
			operationLayout=(LinearLayout)rootView.findViewById(R.id.operation);
			layout=(LinearLayout)rootView.findViewById(R.id.layout);
			TextView addition=(TextView)operationLayout.findViewById(R.id.addition);
			TextView multiplication=(TextView)operationLayout.findViewById(R.id.multiplication);
			TextView cancel=(TextView)operationLayout.findViewById(R.id.cancel);
			final SecretPopup popup = new SecretPopup(InitiatorActivity.context);
			popup.setTitle(getResources().getString(R.string.initiator));
			popup.setContain(getResources().getString(R.string.s));
			content=(LinearLayout)rootView.findViewById(R.id.initiatorContent);
			addition.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View arg0) {
					isSendFirstMessage=false;
					isAddition=true;
					popup.show(content);

				}

			});
			multiplication.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View arg0) {
					isSendFirstMessage=false;
					isAddition=false;
					popup.show(content);

				}


			});
			cancel.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View arg0) {
					reset();
					operationLayout.setVisibility(View.GONE);
					layout.setVisibility(View.VISIBLE);

				}


			});
			popup.setOnPopupTextEnteredListener(new OnPopupTextEnteredListener(){

				@Override
				public void onItemEntered(String value) {
					if(isAddition){
						makeAdditionOperation(value);
					}
					else{
						makeMultiplication(value);
					}
				}
			});

			return rootView;
		}

		protected void makeMultiplication(String value) {
			int s=Integer.parseInt(value)%100;
			Random ran = new Random();

			s1 = ran.nextInt(1000);
			s2=ran.nextInt(1000);
			s3=(s-s1-s2)%1000;
			if (s3<0) s3 += 1000;
			//isSecond,is initiator, is watcher, initiator address,the watcher address,the value,isAddition,result,isSenderInitiator

			String message="1,0,0,"+mBluetoothAdapter.getAddress()+","+devices.get(watcherDevice)+","+String.valueOf(s2)+","+"0"+","+String.valueOf(0)+","+"1"+","+String.valueOf(s1);
			sendMessage(message);
		}


	}
	int s1=0;
	int s2=0;
	int s3=0;
	int t1=0;
	int t2=0;
	int t3=0;
	protected  void makeAdditionOperation(String value) {
		int s=Integer.parseInt(value)%100;
		Random ran = new Random();
		s1 = ran.nextInt(100);
		s2=ran.nextInt(100);
		s3=(s-s1-s2)%100;
		if (s3<0) s3 += 100;
		//isSecond,is initiator, is watcher, initiator address,the watcher address,the value,isAddition,result,isSenderInitiator
		String message="1,0,0,"+mBluetoothAdapter.getAddress()+","+devices.get(watcherDevice)+","+String.valueOf(s2)+","+"1"+","+String.valueOf(0)+","+"1";
		sendMessage(message);


	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if(requestCode == REQUEST_ENABLE_BT){
			CheckBlueToothState();
		}if (requestCode == REQUEST_Turn_On_Discoverable){
			if(resultCode == RESULT_OK){
			}
			else if (resultCode == RESULT_CANCELED){
			}
		}
		if(requestCode ==  REQUEST_CONNECT_DEVICE){

			if (resultCode == Activity.RESULT_OK) {

				ArrayList<String> address = data.getExtras().getStringArrayList(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
				BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address.get(0));
				BluetoothDevice device2 = mBluetoothAdapter.getRemoteDevice(address.get(1));

				devices.add(device);
				devices.add(device2);
				Random ran = new Random();
				secondDevice = ran.nextInt(2);

				if(secondDevice==0){
					watcherDevice=1;
				}
				else{
					watcherDevice=0;
				}
				mChatService.setInitiatorAddress(mBluetoothAdapter.getAddress());
				mChatService.setWatcherAddress(address.get(watcherDevice));
				mChatService.setInitiator(true);
				mChatService.connectSecond(devices.get(secondDevice),true);
			}
		}
	}


	private void CheckBlueToothState(){
		if (mBluetoothAdapter == null){

		}else{
			if (mBluetoothAdapter.isEnabled()){
				if (mChatService == null) setupChat();
				if(mBluetoothAdapter.isDiscovering()){
					bluetooth.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_location_found));
				}else{

					bluetooth.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_location_off));
				}
			}else{

				Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
			}
		}
	}
	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {

		public void onReceive(Context context, Intent intent) {

			String action = intent.getAction();
			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				mArray.add(device.getName() + "\n" + device.getAddress());
			}
		}
	};

	private final BroadcastReceiver modeChangedReceiver = new BroadcastReceiver(){

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED.equals(action)) {

				int mode = intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, 
						BluetoothAdapter.ERROR);
				switch(mode){
				case BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE:
					bluetooth.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_location_found));
					bluetoothLayout.setEnabled(false);
					bluetoothLayout.setClickable(false);
					textBlu.setText(R.string.discoverable);
					break;
				case BluetoothAdapter.SCAN_MODE_CONNECTABLE:
					bluetooth.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_location_off));
					bluetoothLayout.setEnabled(true);
					bluetoothLayout.setClickable(true);
					textBlu.setText(R.string.bluetooth);
					break;
				case BluetoothAdapter.SCAN_MODE_NONE:
					bluetooth.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_location_off));
					bluetoothLayout.setEnabled(true);
					bluetoothLayout.setClickable(true);
					textBlu.setText(R.string.bluetooth);
					break;
				}

			}
		}};

		private final void setStatus(int resId) {
			final ActionBar actionBar = getActionBar();
			actionBar.setSubtitle(resId);
		}

		private final void setStatus(CharSequence subTitle) {
			final ActionBar actionBar = getActionBar();
			actionBar.setSubtitle(subTitle);
		}
		public static void sendMessage(String message) {

			if (mChatService.getState() != BluetoothChatService.STATE_CONNECTED) {
				Toast.makeText(InitiatorActivity.context, R.string.not_connected, Toast.LENGTH_SHORT).show();
				return;
			}
			if (message.length() > 0) {
				byte[] send = message.getBytes();
				mChatService.write(send);
				mOutStringBuffer.setLength(0);

			}
		}

}
