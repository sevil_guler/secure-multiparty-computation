/*
 * Copyright (c) 2014 Sevil GULER
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



package com.ut.researchseminar.activities;

import java.util.Random;

import com.example.researchseminar.R;
import com.ut.research.seminar.interfaces.OnCustomAlertViewClickListener;
import com.ut.researchseminar.utils.CustomAlertView;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DiagramActivity extends Activity{
	Button peer1;
	Button peer2;
	Button peer3;
	ImageView arrow_frompeer1topeer2;
	ImageView arrow_frompeer2topeer1;
	TextView fromPeer1ToPeer2;
	TextView fromPeer2ToPeer1;
	Button fromPeer1ToPeer3;
	Button fromPeer2ToPeer3;
	EditText s_editText;
	EditText t_editText;
	TextView ok;
	boolean isAddition;
	int s1_t1=-1;
	int s2_t2=-1;
	int w1=-1;
	int w2=-1;
	int w3=-1;
	int s1=0;
	int s2=0;
	int s3=0;
	int t1=0;
	int t2=0;
	int t3=0;
	int allResult=-1;
	int u3=0;
	int count=0;
	LinearLayout content;
	static Context context;
	TextView sandt;
	LinearLayout peer1_peer2_area;
	TextView fromPeer1orPeer2;
	TextView sent;
	TextView shares;
	TextView own;
	TextView done;
	TextView type;
	TextView part;
	TextView partValue;
	LinearLayout peer3_area;
    int which_pair=-1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.diagram_activity);
		content=(LinearLayout)this.findViewById(R.id.content);
		context=this.getApplication().getApplicationContext();
		if(getIntent()!=null){
			allResult=getIntent().getExtras().getInt(InitiatorActivity.RESULT);
			isAddition=getIntent().getExtras().getBoolean(InitiatorActivity.IS_ADDITION);

		}
		else{
			Toast.makeText(content.getContext(), "NULL", Toast.LENGTH_LONG).show();
		}
		peer1_peer2_area=(LinearLayout)this.findViewById(R.id.peer1_peer2_area);
		peer3_area= (LinearLayout)this.findViewById(R.id.peer3_area);
		fromPeer1orPeer2=(TextView)this.findViewById(R.id.fromPeer1orPeer2);
		sent=(TextView)this.findViewById(R.id.sent);
		shares=(TextView)this.findViewById(R.id.shares);
		own=(TextView)this.findViewById(R.id.own);
		done=(TextView)this.findViewById(R.id.done);
		type=(TextView)this.findViewById(R.id.type);
		part=(TextView)this.findViewById(R.id.part);
		partValue=(TextView)this.findViewById(R.id.partValue);
		done.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				peer3_area.setVisibility(View.GONE);
				peer1_peer2_area.setVisibility(View.GONE);
				done.setVisibility(View.GONE);
				peer1.setPressed(false);
				peer2.setPressed(false);
				peer3.setPressed(false);
				peer1.setSelected(false);
				peer2.setSelected(false);
				peer3.setSelected(false);

			}
		});

		peer1=(Button)this.findViewById(R.id.peer1);
		peer2=(Button)this.findViewById(R.id.peer2);
		peer3=(Button)this.findViewById(R.id.peer3);
		arrow_frompeer1topeer2=(ImageView)this.findViewById(R.id.arrow_fromPeer1ToPeer2);
		arrow_frompeer2topeer1=(ImageView)this.findViewById(R.id.arrow_fromPeer2ToPeer1);
		fromPeer1ToPeer2=(TextView)this.findViewById(R.id.fromPeer1ToPeer2);
		fromPeer2ToPeer1=(TextView)this.findViewById(R.id.fromPeer2ToPeer1);
		fromPeer1ToPeer3=(Button)this.findViewById(R.id.fromPeer1ToPeer3);
		fromPeer2ToPeer3=(Button)this.findViewById(R.id.fromPeer2ToPeer3);
		//ClickListeners
		arrow_frompeer1topeer2.setOnClickListener(showPeer1TranscriptListener);
		fromPeer1ToPeer2.setOnClickListener(showPeer1TranscriptListener);
		peer1.setOnClickListener(showPeer1TranscriptListener);
		fromPeer1ToPeer3.setOnClickListener(showPeer1TranscriptListener);

		arrow_frompeer2topeer1.setOnClickListener(showPeer2TranscriptListener);
		fromPeer2ToPeer1.setOnClickListener(showPeer2TranscriptListener);
		peer2.setOnClickListener(showPeer2TranscriptListener);
		fromPeer1ToPeer3.setOnClickListener(showPeer2TranscriptListener);
		sandt=(TextView)this.findViewById(R.id.sandt);
		peer3.setOnClickListener(showPeer3TranscriptListener);
		ok=(TextView)this.findViewById(R.id.ok);

		if(!isAddition){
			sandt.setText(getResources().getString(R.string.mult_diagram,allResult));
			mixTheValuesMult();
			ok.setOnClickListener(multiplicationOKClickListener);
			type.setText(getResources().getString(R.string.multiplication));
		}
		else{
			sandt.setText(getResources().getString(R.string.addition_diagram,allResult));
			type.setText(getResources().getString(R.string.addition));
			mixTheValues();
			ok.setOnClickListener(additionOKClickListener);
		}


	}
	protected void mixTheValues() {
		Random ran = new Random();

		int s=ran.nextInt(allResult);
		s1 = ran.nextInt(100);
		s2=ran.nextInt(100);
		s3=(s-s1-s2)%100;
		if (s3<0) s3 += 100;
		int t=allResult-s;
		t1 = ran.nextInt(100);
		t2=ran.nextInt(100);
		t3=(t-t1-t2)%100;
		if (t3<0) t3 += 100;
		s2_t2=t2+s2;
		s1_t1=t1+s1;
		u3=(s3+t3)%100;
		allResult=(s2_t2+s1_t1+u3)%100;
		fromPeer1ToPeer2.setText("s2 = "+ String.valueOf(s2));
		fromPeer2ToPeer1.setText("t1 = "+ String.valueOf(t1));
		fromPeer1ToPeer3.setText("(u1,s3) \n=\n("+ String.valueOf(s1_t1)+","+ String.valueOf(s3)+")" );
		fromPeer2ToPeer3.setText("(u2,t3) \n=\n("+ String.valueOf(s2_t2)+","+ String.valueOf(t3)+")" );
		count++;
		if(count==4){
			customAlertShow();
		}
		update();
	}

	OnClickListener showPeer1TranscriptListener= new OnClickListener() {

		@Override
		public void onClick(View v) {
			which_pair=1;
			peer1.setPressed(true);
			peer2.setPressed(false);
			peer3.setPressed(false);
			peer1.setSelected(true);
			peer2.setSelected(false);
			peer3.setSelected(false);
			done.setVisibility(View.VISIBLE);
			updateP1();
			peer1_peer2_area.setVisibility(View.VISIBLE);
            peer3_area.setVisibility(View.GONE);

		}
	};
	public void updateP1(){
		if(isAddition){
			fromPeer1orPeer2.setText("Peer 2    -->   t1 = ");
			sent.setText(String.valueOf(t1));
			part.setText("u1           -->   s1 + t1 = ");
			partValue.setText(String.valueOf(s1)+" + " + String.valueOf(t1) +" = " + String.valueOf(s1+t1));
		}
		else{
			fromPeer1orPeer2.setText("Peer 2    -->   (t1,t3) = ");
			sent.setText("("+String.valueOf(t1)+","+String.valueOf(t3)+")");
			part.setText("w1           -->   s1*t1 + s1*t3 + s3*t1 = ");
			partValue.setText(String.valueOf(w1));
		}
		shares.setText("(s1,s2,s3) = ");
		own.setText("("+ String.valueOf(s1)+","+String.valueOf(s2)+ ","+String.valueOf(s3)+")");
	}
	
	OnClickListener additionOKClickListener= new OnClickListener() {

		@Override
		public void onClick(View v) {
			mixTheValues();

		}
	};
	OnClickListener multiplicationOKClickListener= new OnClickListener() {

		@Override
		public void onClick(View v) {
			mixTheValuesMult();

		}
	};
	OnClickListener showPeer2TranscriptListener= new OnClickListener() {

		@Override
		public void onClick(View v) {
			which_pair=2;
			peer1.setPressed(false);
			peer2.setPressed(true);
			peer3.setPressed(false);
			peer1.setSelected(false);
			peer2.setSelected(true);
			peer3.setSelected(false);
			done.setVisibility(View.VISIBLE);
			updateP2();
			peer1_peer2_area.setVisibility(View.VISIBLE);
			peer3_area.setVisibility(View.GONE);

		}
	};
	public void updateP2(){
		if(isAddition){
			fromPeer1orPeer2.setText("Peer 1    -->   s2 = ");
			sent.setText(String.valueOf(s2));
			part.setText("u2           -->   s2 + t2 = ");
			partValue.setText(String.valueOf(s2)+" + " + String.valueOf(t2) +" = " + String.valueOf(s2+t2));
		}
		else{
			fromPeer1orPeer2.setText("Peer 1    -->   (s2,s1) = ");
			sent.setText("("+String.valueOf(s3)+","+String.valueOf(s2)+")");
			part.setText("w2           -->   s2*t2 + s2*t1 + s1*t2 = ");
			partValue.setText(String.valueOf(w2));}

		shares.setText("(t1,t2,t3) = ");
		own.setText("("+ String.valueOf(t1)+","+String.valueOf(t2)+ ","+String.valueOf(t3)+")");
	}
	View inflated;
	TextView p1_send;
	TextView p2_send;
	TextView p1_result;
	TextView p2_result;
	TextView p3_result;
	TextView all_result;
	OnClickListener showPeer3TranscriptListener= new OnClickListener() {
		@Override
		public void onClick(View v) {
			which_pair=3;
			peer1.setPressed(false);
			peer2.setPressed(false);
			peer3.setPressed(true);
			peer1.setSelected(false);
			peer2.setSelected(false);
			peer3.setSelected(true);
			peer1_peer2_area.setVisibility(View.GONE);
			done.setVisibility(View.VISIBLE);
			
			LayoutInflater li = LayoutInflater.from(context);
			if(inflated==null){
			if(isAddition){
				inflated= li.inflate(R.layout.transcript, null, false);
			}
			else{
				inflated= li.inflate(R.layout.transcript_for_multiplication, null, false);
			}
			peer3_area.addView(inflated);
			}
			
			LinearLayout askForSandT=(LinearLayout)inflated.findViewById(R.id.askForSandT);
			askForSandT.setVisibility(View.GONE);
			TextView ok=(TextView)inflated.findViewById(R.id.ok);
			ok.setVisibility(View.GONE);
			
			p1_send=(TextView)inflated.findViewById(R.id.p1_send);
			p2_send=(TextView)inflated.findViewById(R.id.p2_send);
			p1_result=(TextView)inflated.findViewById(R.id.p1_result);
			p2_result=(TextView)inflated.findViewById(R.id.p2_result);
			p3_result=(TextView)inflated.findViewById(R.id.u3);
			all_result=(TextView)inflated.findViewById(R.id.s_t);
			updateP3();
			peer3_area.setVisibility(View.VISIBLE);

		}


	};
	public void updateP3(){
		if(isAddition){
			p1_send.setText(String.valueOf(s3));
			p2_send.setText(String.valueOf(t3));
			p1_result.setText(String.valueOf(s1_t1));
			p2_result.setText(String.valueOf(s2_t2));
			u3=(s3+t3)%100;
			p3_result.setText(String.valueOf(u3));
			allResult=(s2_t2+s1_t1+u3)%100;
			all_result.setText(String.valueOf(allResult));
		}
		else{
			p1_send.setText('('+String.valueOf(s3)+','+String.valueOf(s2)+')');
			p2_send.setText('('+String.valueOf(t3)+','+String.valueOf(t2)+')');
			p1_result.setText(String.valueOf(w1));
			p2_result.setText(String.valueOf(w2));
			w3=(s3*t3 + s3*t2 + s2*t3)%1000;
			p3_result.setText(String.valueOf(w3));
			allResult=(w2+w1+w3)%1000;
			all_result.setText(String.valueOf(allResult));
		}
	}
	protected void mixTheValuesMult() {
		Random ran = new Random();
		int s=-1;
		while(true){
			int temp_s=ran.nextInt(allResult);
			if(temp_s!=0 && allResult%temp_s==0){
				s=temp_s;
				break;
			}
		}
		s1 = ran.nextInt(1000);
		s2=ran.nextInt(1000);
		s3=(s-s1-s2)%1000;
		if (s3<0) s3 += 1000;
		int t=allResult/s;
		t1 = ran.nextInt(1000);
		t2=ran.nextInt(1000);
		t3=(t-t1-t2)%1000;
		if (t3<0) t3 += 1000;
		w1=(s1*t1+s1*t3+s3*t1)%1000;
		w2=(s2*t2+s2*t1+s1*t2)%1000;
		w3=(s3*t3 + s3*t2 + s2*t3)%1000;
		allResult=(w2+w1+w3)%1000;
		fromPeer1ToPeer2.setText("(s2,s1) = ("+ String.valueOf(s2)+","+String.valueOf(s1)+")");
		fromPeer2ToPeer1.setText("(t1,t3) = ("+ String.valueOf(t1)+","+String.valueOf(t3)+")");
		fromPeer1ToPeer3.setText("(w1,s3,s2) \n=\n("+ String.valueOf(w1)+","+String.valueOf(s3)+","+String.valueOf(s2)+")");
		fromPeer2ToPeer3.setText("(w2,t2,t3) \n=\n("+ String.valueOf(w2)+","+String.valueOf(t2)+","+String.valueOf(t3)+")");
		count++;
		if(count==4){
			customAlertShow();
		} 
		update();
		
	}

	private void update() {
		switch(which_pair){
		case 1:
			updateP1();
			break;
		case 2:
			updateP2();
			break;
		case 3:
			updateP3();
			break;
		
		}
	}
	protected void customAlertShow() {
		CustomAlertView customAlertView = new CustomAlertView(this,0);
		customAlertView.setTitle(getResources().getString(R.string.guesser));
		if(isAddition){
			customAlertView.setWarn(getResources().getString(R.string.endOfTheGame,allResult));
		}

		else{
			customAlertView.setWarn(getResources().getString(R.string.endOfTheGameMult,allResult));
		}
		customAlertView.setOnCustomAlertViewClickListener(new OnCustomAlertViewClickListener() {

			@Override
			public void onItemClickLeave() {
				finish();
			}

		});
		customAlertView.show(content);
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		s1_t1=-1;
		s2_t2=-1;
		w1=-1;
		w2=-1;
		w3=-1;
		isAddition=false;
		count=0;
	}
}