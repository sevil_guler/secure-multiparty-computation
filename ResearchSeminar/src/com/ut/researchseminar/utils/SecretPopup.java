package com.ut.researchseminar.utils;

import com.example.researchseminar.R;
import com.ut.research.seminar.interfaces.OnPopupTextEnteredListener;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

public class SecretPopup {

	private PopupWindow popupW;
	private OnPopupTextEnteredListener onPopupTextEnteredListener;
	private LinearLayout root;
	TextView title;
	TextView contain;

	public SecretPopup(Context context) {
		super();
		this.popupW = new PopupWindow(context);
		popupW.setFocusable(true);
		root = (LinearLayout)(((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.popupwithtextview, null));
		title=(TextView)root.findViewById(R.id.title);
		contain=(TextView)root.findViewById(R.id.contain);
	}

	public void setTitle(String title) {
		this.title.setText(title);
	}

	public void setContain(String contain) {
		this.contain.setText(contain);
	}

	public void setClickListener()	{
		
		TextView ok=(TextView)root.findViewById(R.id.ok);
		final EditText ed=(EditText)root.findViewById(R.id.editText);
		ok.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				popupW.dismiss();
				String value=ed.getText().toString();
				onPopupTextEnteredListener.onItemEntered(value);
			}
		});
		
	}
	

	public void show(View v)	{
		popupW.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
		popupW.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
		popupW.setContentView(root);
		root.measure(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
		popupW.showAtLocation(v, Gravity.CENTER, 0, 0);
	}
	public void setOnPopupTextEnteredListener(OnPopupTextEnteredListener onPopupTextEnteredListener)	{
		this.onPopupTextEnteredListener = onPopupTextEnteredListener;
		setClickListener();
	}
}
