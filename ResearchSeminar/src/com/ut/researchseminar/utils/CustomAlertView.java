package com.ut.researchseminar.utils;

import com.example.researchseminar.R;
import com.ut.research.seminar.interfaces.OnCustomAlertViewClickListener;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

public class CustomAlertView {

	private PopupWindow popupW;
	private OnCustomAlertViewClickListener onCustomAlertViewClickListener;
	private LinearLayout rootV;	
	TextView title;
	TextView warn;
	TextView leave;
	int number;

	public CustomAlertView(Context context,int number) {
		super();
		this.popupW = new PopupWindow(context);
		this.popupW .setFocusable(true);	
		rootV = (LinearLayout) ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_alert_view, null);
		title=(TextView)rootV.findViewById(R.id.title);
		warn=(TextView)rootV.findViewById(R.id.warningMessage);
		leave=(TextView) rootV.findViewById(R.id.leave);
		this.number=number;
		if(number==0){
			leave.setVisibility(View.GONE);
		}
	}

	public void setTitle(String title) {
		this.title.setText(title);
	}

	public void setWarn(String warn) {
		this.warn.setText(warn);
	}

	public void setClickListener()	{
		
		TextView ok=(TextView)rootV.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				popupW.dismiss();
				if(number==0){
					onCustomAlertViewClickListener.onItemClickLeave();
				}
				popupW=null;

			}
		});
		
		leave.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				popupW.dismiss();
				onCustomAlertViewClickListener.onItemClickLeave();

			}
		});
	}

	public void show(View v)	{
		popupW.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
		popupW.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
		popupW.setContentView(rootV);
		rootV.measure(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
		popupW.showAtLocation(v, Gravity.CENTER, 0, 0);
	}
	public void setOnCustomAlertViewClickListener(OnCustomAlertViewClickListener onCustomAlertViewClickListener)	{
		this.onCustomAlertViewClickListener = onCustomAlertViewClickListener;
		setClickListener();
	}
}
