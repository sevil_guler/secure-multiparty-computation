package com.ut.researchseminar.utils;

import com.example.researchseminar.R;
import com.ut.research.seminar.interfaces.OnPopupTextEnteredListener;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

public class TranscriptPopup {

	private PopupWindow popuupW;
	private OnPopupTextEnteredListener onPopupTextEnteredListener;
	private LinearLayout rootV;

	TextView p1_send;
	TextView p2_send;
	TextView p1_result;
	TextView p2_result;
	TextView p3_result;
	TextView all_result;
	boolean fromDiagramActivity=false;
	LinearLayout askForSandT;

	public TranscriptPopup(Context context,boolean isAddition,boolean fromDiagramActivity) {
		super();
		this.popuupW = new PopupWindow(context);
		popuupW.setFocusable(true);
		this.fromDiagramActivity=fromDiagramActivity;
		LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if(isAddition){
			rootV = (LinearLayout) layoutInflater.inflate(R.layout.transcript, null);
		}
		else{
			rootV = (LinearLayout) layoutInflater.inflate(R.layout.transcript_for_multiplication, null);
		}
		LinearLayout popupLayout=(LinearLayout)rootV.findViewById(R.id.popupLayout);
		popupLayout.setBackgroundColor(context.getResources().getColor(R.color.popup_background));
		popupLayout.setPadding(30, 30, 30, 30);
		askForSandT=(LinearLayout)rootV.findViewById(R.id.askForSandT);
		if(this.fromDiagramActivity){
			askForSandT.setVisibility(View.GONE);
			TextView ok=(TextView)rootV.findViewById(R.id.ok);
			ok.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					popuupW.dismiss();	
				}
			});
		}
		p1_send=(TextView)rootV.findViewById(R.id.p1_send);
		p2_send=(TextView)rootV.findViewById(R.id.p2_send);
		p1_result=(TextView)rootV.findViewById(R.id.p1_result);
		p2_result=(TextView)rootV.findViewById(R.id.p2_result);
		p3_result=(TextView)rootV.findViewById(R.id.u3);
		all_result=(TextView)rootV.findViewById(R.id.s_t);
	}


	public TextView getP1_send() {
		return p1_send;
	}


	public void setP1_send(String p1_send) {
		this.p1_send.setText(p1_send);
	}


	public void setP2_send(String p2_send) {
		this.p2_send.setText(p2_send);
	}


	public TextView getP1_result() {
		return p1_result;
	}


	public void setP1_result(String p1_result) {
		this.p1_result.setText(p1_result);
	}




	public void setP2_result(String p2_result) {
		this.p2_result.setText(p2_result);
	}


	public void setP3_result(String p3_result) {
		this.p3_result.setText(p3_result);
	}


	public void setAll_result(String all_result) {
		this.all_result.setText(all_result);
	}


	public OnPopupTextEnteredListener getOnPopupTextEnteredListener() {
		return onPopupTextEnteredListener;
	}


	public void setClickListener()	{

		TextView ok=(TextView)rootV.findViewById(R.id.ok);
		final EditText ted=(EditText)rootV.findViewById(R.id.t_editText);
		final EditText sed=(EditText)rootV.findViewById(R.id.s_editText);
		ok.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				popuupW.dismiss();
				String value=sed.getText().toString()+","+ted.getText().toString();
				onPopupTextEnteredListener.onItemEntered(value);

			}

		});

	}

	public void show(View v)	{
		popuupW.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
		popuupW.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
		popuupW.setContentView(rootV);
		rootV.measure(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
		popuupW.showAtLocation(v, Gravity.CENTER, 0, 0);
	}
	public void setOnPopupTextEnteredListener(OnPopupTextEnteredListener onPopupTextEnteredListener)	{
		this.onPopupTextEnteredListener = onPopupTextEnteredListener;
		setClickListener();
	}
}
