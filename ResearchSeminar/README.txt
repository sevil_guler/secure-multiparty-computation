**********************************************************************************************************
					   	 
					   	 UNIVERSITY OF TARTU
					 RESEARCH SEMINAR 2014/2015 SPRING
				
**********************************************************************************************************
This android application uses secure multiparty computation approach which is secret sharing based.
Secret sharing uses n computation parties which are independent and each of them can compute an agreed
function of their inputs in a secure way,where security means guaranteeing the privacy 
of the parties' inputs.The app makes bluetooth connection for communication and shares secret data.

In this project 3 computation parties were used.Every party runs Android application developed
by this project which ensures each of them has just one role:


1. Initiator party (IP): The initial owner of the secret data. Initiator party initiates the
application and chooses other two parties for computation. The initiator also chooses the
function either addition or multiplication.
2. Helper party (HP): The party who helps to evaluate a function on secret data.
3. Guesser party (GP): The party who is interested the result of the computation. Guesser is
the one just who is able to see the result and try to find inputs.


Purpose of this project is developing an educational mobile app to demonstrate secure multiparty
computation. The app aims providing privacy of inputs by showing GP cannot deduce anything
from the shares it gets. Since the connection channel is neither authorized nor encrypted it is
important to know that the app achieve security only for a passive adversary setting, without the
possibility to enchance it to active adversary settings. The system provide one secret sharing
scheme: additive which uses following protocols


USAGE:

This application supports Android 4.0 version (API Level 14) and above which 
means the device that wants to run this mobile application should have at 
least Android 4.0 version as an operating system. All the tests are made on Nexus 5(API Level 18). 
We don't guarantee perfect result for other devices.

Make sure you have 3 devices which has bluetooth support. Every party should install that application.
User has responsibility of choosing devices those have this app. Otherwise, app will not work properly.

WARNING:

This software includes code from The Android Open Source Project.

BluetoothChat Code was modified by Sevil GULER. Changes:

Socket type removed --> All connection is done by using secure socket type.
Many control state added 
Many functions which achieves main aim are added
Some comments are deleted
Some controls statements which are not necessary for this app were removed
Loging removed
For selecting devices to connect limitation is added --> (Just two)
Since it is the most important things for this app repeatedly trying to connect the device is added --> It rarely causes the app crashing if it fails too much



 

